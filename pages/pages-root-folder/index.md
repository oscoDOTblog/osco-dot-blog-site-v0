---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header:
  image_fullwidth: full/saop-1.jpg
widget1:
  title: "Stories"
  url: 'blog/the-grand-challenge/'
  image: widgets/tgc-v1-302x182.jpg
  text: 'VR fantasy stories I am currently writing.'
widget2:
  title: "Blog"
  url: 'blog/archive/'
  image: widgets/ruby-palace-302x182.jpg
  text: 'My weekly musings and project updates.'
widget3:
  title: "Newsletter"
  url: 'https://news.osco.blog/'
  image: widgets/forest-house-k4-302x182.jpg
  text: 'Stay up-to-date with all things Osco!'
#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#
callforaction:
  url: https://news.osco.blog/
  text: Sign up for my newsletter! ›
  style: alert
permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>
