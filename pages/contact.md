---
layout              : page
title               : "Contact"
meta_title          : "Contact and use our contact form"
subheadline         : "Email is cringe, but everyone has one so here we are"
teaser              : "Got any questions or business inquiries?"
permalink           : "/contact/"
header:
   image_fullwidth  : "header/contact-1500x300.jpg"
---
Shoot me an email and I'll get back to you ASAP!

### Email
```
osco+blog@osco.blog
```
### Socials
* **BlueSky** - [osco.bsky.social](https://bsky.app/profile/osco.bsky.social)
* **Facebook** - [osco.blog](https://www.facebook.com/osco.blog/)
* **Instagram** - [oscoDOTblog](https://www.instagram.com/oscodotblog/)
* **LinkedIn** - [oscoDOTblog](https://www.linkedin.com/in/oscodotblog/)
* **Mastodon** - [@oscoDOTblog@mastodon.social ](https://mastodon.social/@oscoDOTblog)
* **Odysee** - [osco](https://odysee.com/@Osco)
* **Reddit** - [oscoDOTblog](https://www.reddit.com/user/oscoDOTblog
)
* **TikTok** - [oscoDOTblog](https://www.tiktok.com/@essencetankThreads)
* **Threads** - [oscoDOTblog](https://www.threads.net/@oscodotblog)
* **Twitter (X)** - [oscoDOTblog](https://twitter.com/oscoDOTblog)
* **YouTube** - [oscoDOTblog](https://www.youtube.com/@oscoDOTblog/)



