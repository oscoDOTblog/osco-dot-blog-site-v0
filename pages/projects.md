---
permalink: /projects/
layout: page
title: "Projects"
sitemap: false
subheadline         : "Begin your journey into the metaverse"
# teaser              : "A storm wakes on the horizon"
header:
   image_fullwidth  : "header/projects-1500x300.jpg"
---
{% for project in site.data.projects %}
  <div>
    <a href="{{ project.url }}">
      <img src="{{ project.img }}">
    </a>
  </div>
  **[{{ project.name }}]({{ project.url }})** - {{ project.about }}
{% endfor %}

