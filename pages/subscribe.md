---
permalink: /subscribe/
layout: page
title: "Subscribe/Sponsor/Donate"
sitemap: false
subheadline         : "Welcome home"
teaser              : "Are you ready to join Littlewood Town?"
header:
   image_fullwidth  : "header/subscribe-1500x300.jpg"
---
## Monthly Tiers 
You can subscribe monthly for $5 with the following platforms and get lots of cool perks!

<!-- Bitcoin -->
<!-- [![bitcoin](https://img.shields.io/badge/Bitcoin-000?style=for-the-badge&logo=bitcoin&logoColor=white
)](https://ko-fi.com/benbusby) TODO -->
<!-- Ko-fi -->
[![ko-fi](https://img.shields.io/badge/ko%E2%80%93fi-00b9fe?style=for-the-badge&logo=ko-fi&logoColor=white)](https://ko-fi.com/osco/tiers)
<!-- LiberaPay -->
[![liberapay](https://img.shields.io/badge/liberapay-ffd513?style=for-the-badge&logo=liberapay&logoColor=black)](https://en.liberapay.com/osco)
<!-- Patreon -->
[![patreon](https://img.shields.io/badge/Patreon-F96854?style=for-the-badge&logo=patreon&logoColor=white
)](https://www.patreon.com/osco)

<!-- ### Moonlit Black Cats ($5)
<div><img src="/images/full/sao-black-cats.jpg" width="480" height="270"></div>

> "I believe that in this world, there's someone out there who will understand and accept me for who I am, even with all my flaws."
<cite>Sachi</cite>

* Join an exclusive discord community and get a unique role!
* Your name in the end credits for any content I create~ -->

<!-- ### Aincrad Liberation Squad ($10) -->
<!-- <div><img src="/images/full/sao-als.jpg"></div> -->
<!-- >"In this game, we have to fight to survive. But we can still choose how we fight."
<cite>Kibaou</cite> -->

<!-- * All previous rewards upgraded, and... -->
<!-- * Exclusive in-game cosmetics for [Atemosta WebXR Content!](https://atos.to) -->
<!-- * You can join the public monthly gaming nights! 
   * Will most likely be affordable or free games that can host many people at once -->

<!-- ### Divine Dragons Alliance ($20) -->
<!-- <div><img src="/images/full/sao-dda.jpg"></div> -->
<!-- > "We may not be the strongest players, but together, we can accomplish great things."
<cite>Diavel</cite>
* All previous rewards upgraded, and...
* You will get sneak peaks of the upcoming content!
* You can join my goofy guilds across any videogames I'm currently playing 
   * Guild Wars 2, Street Fighter 6, Zenith VR, the list grows on and on... -->

<!-- ### Knights of the Blood Oath ($50) -->
<!-- <div><img src="/images/full/sao-kotbo.jpg"></div> -->
<!-- * All previous rewards upgraded, and...
* You can join our private weekly gaming nights! 
   * These will be more intimate and probably something like co-op in VR
* You can vote to shape the outcome of developing stories! -->

<!-- ### Fuurinkazan ($100) -->
<!-- <div><img src="/images/full/sao-fuurinkazan.jpg"></div> -->
<!-- * All previous rewards upgraded, and...
* Once a month, you'll get an hour with me for a coworking session!
   * We can build something together, play a videogame, or just shoot the breeze, up to you!  -->

<!-- ## Lifetime Milestone Tiers
I keep track of amazing people like you! 

Here are some extra things that are in the works...

* Every $100, you can name a random NPC in an upcoming story or virtual reality experience!
* Every $250, you can name a random special item in an upcoming story or virtual reality experience!
* Every $500, you can name a legendary hero or heroine in an upcoming story or virtual reality experience!
* Every $1000, you can name a town in an upcoming story or virtual reality experience!
* Every $1250, you will be invited to the yearly oscoDOTblog conference. 
   * I literally have no idea what we will do together, but it will be ***DOPE***. 
   * If you reach this tier multiple times in a year, you will get invites for any upcoming conventions in the subsequent years!
* Every $2500, you can name a major hub city in an upcoming story or virtual reality experience!
* Every $5000,  you can name a deity in an upcoming story or virtual reality experience!
* Every $10,000, I will personally fly to your house and give you a headpat. 
   * Between me leaving my house and me getting to your house, I will come up with a better reward than just a headpat for $10k lol -->

## One-Time Donations
<!-- Be sure to send an email to `osco+donate@osco.blog` if you are still interested in any of the rewards above! -->

<!-- LiberaPay -->
[![ko-fi](https://img.shields.io/badge/ko%E2%80%93fi-00b9fe?style=for-the-badge&logo=ko-fi&logoColor=white)](https://ko-fi.com/osco/)
<!-- PayPal -->
[![paypal](https://img.shields.io/badge/PayPal-00457C?style=for-the-badge&logo=paypal&logoColor=white)](https://www.paypal.com/paypalme/atemosta?country.x=US&locale.x=en_US) 
<!-- Stripe -->
<!-- [![stripe](https://img.shields.io/badge/donate-6772e5?style=for-the-badge&logo=stripe&logoColor=white)](https://donate.stripe.com/cN203j095dMG3jq5kk) TODO -->