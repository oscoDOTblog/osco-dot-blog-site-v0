---
layout: page
title: "Not For Money"
subheadline: "Seemingly illogical decisions"
teaser: "Is there something greater in this life than cold, hard cash?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-lisbeth.jpg
    title: full/sao-lisbeth.jpg
    thumb:  thumb/sao-lisbeth.jpg
---
tread carefully