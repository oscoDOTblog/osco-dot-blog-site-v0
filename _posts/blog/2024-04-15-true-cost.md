---
layout: page
title: "True Cost"
subheadline: "The True Journey Begins Now"
teaser: "What happens after you finally achieve your dreams?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/samurai-champloo.jpg
    title: full/samurai-champloo.jpg
    thumb:  thumb/samurai-champloo.jpg
mediaplayer: true
---
A long time ago, I declared to myself that once I quit my corporate job, I would move to El Salvador to 1.) master my Spanish and 2.) focus full-time on my virtual reality and storytelling 

As the month winds down, that reality is finally happening. 

I've had several goodbyes and going away parties with all the people in my life I truly care about, but what I didn't expect was the backlash at me leaving. 

> You don't have to leave, asshole
<cite>A Good Friend</cite>

As an avid fan of fantasy literature, I am all too aware that this time I see a person may very well be the last - life is incredibly fragile, after all. 

I naively assumed everyone else had that same sentiment, and are truly devastated by the idea of me leaving their local spheres for good. 

Part of me wants to stop what I'm doing and stay - some people fight their whole lives to find a place to belong and a community to be part of. Who am I to just throw it away in search of something that may not have been worth it after all?

But this inkling of doubt persists in the back of my mind, telling me I will regret not taking this chance to start anew in a foreign land.

For me personally, I don't take this as a *goodbye forever*.

It's more like a ***see you later***. 

### Trivia
Today's image is from `Samurai Champloo`, a story about meeting and parting ways. 
>Freedom, isn’t something earned through suffering or pushing yourself. You must accept yourself just as you are and live according to the flow of things; that is true freedom. 
<cite>Monk Zuiko</cite>