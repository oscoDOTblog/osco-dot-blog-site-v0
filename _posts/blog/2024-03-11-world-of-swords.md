---
layout: page
title: "World of Swords"
subheadline: "Do Not Regret Anything"
teaser: "Your World of Opportunity"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-world-of-swords.png
    title: full/sao-world-of-swords.png
    thumb:  thumb/sao-world-of-swords.jpg
mediaplayer: true
---
A long time ago, I came across this scene in the first episode of [Sword Art Online](https://vid.puffyan.us/watch?v=6ohYYtxfDCg)

<center>
<video src="https://www.osco.blog/images/full/sao-world-of-swords.mp4" 
  width="480" 
  height="360"
  >
</video>
</center>
<br/>
> In this world, a single blade can take you anywhere you want to go. And even though it's a virtual world, I feel more alive in here than I ever did in the real world.
<cite>Kirito</cite>

This quote has stuck with me with for over a decade, dreaming of this magical place where I too could finally feel like I could achieve and exceed my potential.

As it turns out, it's always been here.

[*It all starts with what you believe you can do.*](/blog/your-mentality-becomes-your-reality)

Funnily enough, I got this realization from my investing mentor, telling me to always be on the lookout of the investing opportunities around me, but especially...

 ***To not regret what has already come to pass.***

<audio src="https://www.osco.blog/images/full/tai-zen-do-not-regret-anything.mp3" type="audio/mp3" controls="controls"></audio>

I used to live in fear of every mistake I have yet to make, and lament over the actions I did not take in the past. 

As I reach the end of [Floor 2](/blog/floor2), I now look forward to what obstacles may come in my way and how much more I have to screw up and learn from. 

This world of opportunity is now my world of swords.