---
layout: page
title: "Your Daily Battery"
# subheadline: "All the decisions we don't realize we make"
teaser: "How much can you really get done in a day?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-nap.jpg
    title: full/sao-nap.jpg
    thumb:  thumb/sao-nap.jpg
---
I came across a video called [**How To Stay Organized When You're Busy**
 by Rachel How](https://www.youtube.com/watch?v=Rnj7SLDy9AY), and it introduced me into a concept called *"Your Daily Battery"*

It's the idea that, just like how your muscles can get exhausted from overuse, so too can your brain, but most people don't consciously realize it.

For me, it's the feeling when you can't make decisions as quickly, and it's usually towards the end of the day.

You are only really at 100% capacity at the beginning of each day (assuming you sleep the required 8 hours comfortably), so ideally, you should plan the day before to get your high priority tasks done in the morning before the mental fatigue wears in. 

Planning also takes part of you daily battery, so you might end up getting stuck obsessing with what to do in each minute of the next day, which is also detrimental to the big picture of getting tasks done. 

The bottom line I'm trying to make natural into my schedule for this year is now

1. Plan, at most, 3 things to get done for tomorrow
2. Take frequent breaks and start meditating more
3. Be thoughtful about how everyday is spent, especially in the morning and evenings (the evenings is when I stay out late with my friends)
4. Driving 

What about you? How do you typically plan and spend your day? 