---
layout: page
title: "One Comment"
subheadline: "The Digital Ripple Effect"
teaser: "What happens when you finally hit that submit button?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/protocol-rain-ed2.png
    title: full/protocol-rain-ed2.png
    thumb:  thumb/protocol-rain-ed3.jpg
mediaplayer: true
---
I've been a lurker on the internet ever since good ol' dial up days.

<center>
<video src="https://www.osco.blog/images/full/aol-dialup.mp4" 
  width="480" 
  height="360"
  >
</video>
</center>
<br/>

In my fear of stirring the pot while growing up, I never left comments anywhere or created posts at all, essentially being a digital ghost.

But starting at the beginning of last year, something changed...

I was asked (not demanded) to lean into the uncertainty of both posting comments and posting content online, and the strangest thing happened.

**Someone reacted.**

A real human on the other side of that dial-up cord. 

![Normal Internet User](/images/full/dog-computer.jpg)

I act terribly old and just never got into the trend of posting every single waking moment of my life to social media, so that usually means I unintentionally ignore most of the opportunities to connect with others online. 

But now that I'm experimenting with curating my posts to make an account with content I would love to see, sometimes I drop a nice message to friends I've met only once, and sometimes friends I have not made at all.

What happens next is always a happy surprise, because somewhere down the line, our paths cross again...

And it's not like strangers meeting up for the first time...

![That Warm Fuzzy Feeling When Your Meet the Friends Waiting for You!](/images/full/protocol-rain-ed3.png)

***But one-time acquaintances now becoming lifetime friends.***


