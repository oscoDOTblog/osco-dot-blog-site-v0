---
layout: page
title: "Turn of the Century"
subheadline: "Great Change Falls Upon Us"
teaser: "Where will you be in your 30s?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/5cm.jpg
    title: full/5cm.jpg
    thumb:  thumb/5cm.jpg
---
For those who don't know, I will be turning 30 at the end of this year!

### Thing I Expected
* Less energy
* Everything hurts and is now a permanent injury 

### Things I Didn't Expect
* Confronting the reality about a life without my parents around
* Despite my friends no longer changing personality wise, their commitments do 


### The Conclusion
The friends bit especially hit hard because I subconsciously assumed everyone would always be around in the same capacity, but that is as far from the truth as possible.

Our 20s are now for figuring out who we want to be, and our 30s is where we start to settle down and plant roots.

I always feel older than I am because it took me so long to figure out what I ***really*** wanted do in life.

And though I am nothing but happy for those friends who are taking to steps to lock-in, I realized now that I am just getting started. 

As much as could just re-live my 20s by just making new friends in that age group (I am *shockingly* good at impersonating a college kid), what I am starting to realize now is that I should instead look for new friends who are also just getting started. 

Starting over is hard, but it is all part of [the worthwhile journey](/blog/the-worthwhile-journey)

### Trivia
What does ***turn of the century*** mean?
> "***Turn of the century***" refers to the period of time when one century is ending and another is beginning. For instance, the turn of the 20th century refers to the period around the years 1890-1910, which marked the transition from the 19th to the 20th century. It is often used to describe a specific era in history characterized by various cultural, social, and technological changes that occur during this time of transition.


Today's banner is from a short film called [5 Centimeters per Second](https://en.wikipedia.org/wiki/5_Centimeters_per_Second).

> The title "***5 Centimeters per Second***" comes from the speed at which cherry blossom petals fall, with petals being a metaphorical representation of humans, reminiscent of the slowness of life and how people often start together but slowly drift into their separate ways.
