---
layout: page
title: "Days Gone Bye"
subheadline: "Losing Track of the Seasons"
teaser: "If you know how much time you had left, what would change?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/days-go-by.png
    title: full/days-go-by.png
    thumb:  thumb/days-go-by.jpg
mediaplayer: true
---
Back when I was a kid, my sense of time was very defined - autumn, winter, and spring were for school, and summer was for vacation and freedom. 

As an adult approaching thirty, I pretty much lost sense of time dilation - the days would feel long, but the years would slash on by, and that ***scared*** me.

I just hit the six year mark at my first and only corporate job out of office, hit a fat raise, [and feel so ***empty*** inside](https://vid.puffyan.us/watch?v=DYvhC_RdIwQ). 

I have a good bit of money saved up... but that's pretty up it. 

If things were different and I owned a home or had a family, I think I would be ecstatic about the idea of getting more money so I can continue to put roots down and support people outside of myself.

But as I noted in [Turn of the Century](/blog/turn-of-the-century), it is not where I want to be - not quite yet. 

With all that being said...

### What's Next?

Since the beginning of last year (my five year mark at my corporate job), I decided I would save up as much money as possible, and leave my job at the six year mark. 

I continually asked myself, *"What would I do I didn't have to work anymore?"*, and the answer always turned into `telling stories in virtual reality`.

Starting next week, I will be temporarily free of the corporate grind, and start exploring what to with the rest of my life. 

I look forward to experiencing the seasons once more 🌸

### Trivia
This week's banner post is from [Amagi Brilliant Park](https://vid.puffyan.us/watch?v=L1SeZtTb_i8) - it's a short, cute, and funny story about a failing amusement park having only 90 days to turn around their operations before being shut down for good. 

<center>
<video src="https://www.osco.blog/images/videos/amagi-kids.mp4" 
  width="480" 
  height="360"
  >
</video>
</center>
<br/>

I admittedly liked it more than I expected (especially their rendition of preschoolers 🤣), so I invite you to give it a shot!

I also recently discovered that I loved to dance - even though I'm not the greatest at it, and [remembered this song](https://vid.puffyan.us/watch?v=aMBKWNqwOpw) which inspired today's blog title. 