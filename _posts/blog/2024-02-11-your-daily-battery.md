---
layout: page
title: "Your Daily Battery"
subheadline: "Managing the Discharge"
teaser: "How much can you 『really』 get done in a day?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-nap.jpg
    title: full/sao-nap.jpg
    thumb:  thumb/sao-nap.jpg
---
I came across a video called [**How To Stay Organized When You're Busy**
 by Rachel How](https://www.youtube.com/watch?v=Rnj7SLDy9AY), and it introduced me into a concept called *"Your Daily Battery"*

It's the idea that, just like how your muscles can get exhausted from overuse, so too can your brain, but most people don't consciously realize it.

For me, it's the observation when I can't make decisions as quickly, and it's usually towards the end of the day.

You are only really at 100% capacity at the beginning of each day (assuming you sleep the required 8 hours comfortably), so you should ideally plan to complete your high priority tasks i the morning before the mental fatigue wears in. 

Planning also takes part of you daily battery, so you might end up getting stuck obsessing with what to do in each minute of the next day, which is also detrimental to the big picture of really『**getting stuff done**』. 

The bottom line I'm trying to make natural into my schedule for this year is now:

* Plan, at most, 3 big things to get done for tomorrow that will move the needle forward for my yearly goals
* Take frequent breaks and start meditating more 
  * Meditating is like a break for you mind! It's... really hard to think about nothing, but VR is starting to help me get into it - more in a future blog post ;^)
* Be thoughtful about how everyday is spent, especially in the morning and evenings 
  * The evenings is when I stay out late with my friends, which can bleed into waking up early the next day...
* Driving is cringe and I want to try it avoid it as much as possible

What about you? How do you typically plan and spend your day? 