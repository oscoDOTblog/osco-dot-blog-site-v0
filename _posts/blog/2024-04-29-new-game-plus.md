---
layout: page
title: "New Game Plus"
subheadline: "Starting a New Save File"
teaser: "If you could start over with everything you know what, what would change?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-new-game.jpg
    title: full/sao-new-game.jpg
    thumb:  thumb/sao-new-game.jpg
mediaplayer: true
---
I fell out of rhythm with posting my project progress because:
* I didn't have [Structure](/blog/structure) in my life 
* I didn't have specific goals I was trying to timebox
* I let myself get too swept up in the moment, especially when dealing with others

***But most importantly***, I had lost the confidence in executing on my projects because I did not feel like I had all the information required to progress. 

Turns out, I never will. 

Nobody *really* knows what we are doing, but the best executors know where they are going and how to achieve the next smallest steps. 

Drawing from my past experiences, but especially looking for help ASAP will definitely be the keys to success for this year. 

### Updates
This is a new section I am baking in per week to keep myself accountable!

Next week, I plan to have the rough story draft of the [The Grand Challenge](/projects) done so I can start baking the characters around them. 

### Trivia 
This blog post's screencap is from the opening sequence of [Sword Art Online's Opening 2](https://vid.puffyan.us/watch?v=vdPPA-2ErW8)

I ***love*** the recurring theme of the series which explores the relationship between our physical and digital selves, and the first 20 seconds beautifully encapsulate that. 