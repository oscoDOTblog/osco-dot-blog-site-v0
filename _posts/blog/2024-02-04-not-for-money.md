---
layout: page
title: "Not For Money"
subheadline: "Seemingly illogical decisions"
teaser: "Is there something greater in this life than cold, hard cash?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-lisbeth.jpg
    title: full/sao-lisbeth.jpg
    thumb:  thumb/sao-lisbeth.jpg
---
I recently came back from [Super MAGFest](https://super.magfest.org/), a 96-hour gaming convention that prides itself on grassroots music and gaming.

One of my new favorite draws for this event in the **Indie Gaming** section, where you can try out some insane demos for game ideas you could never conventionally conceive of.

Almost all of these ideas will never received widespread acclaim or funding, yet all the indie devs I talked to were so passionate about their projects, and almost all of them had still had regular day jobs.

"*Why spend so much time on something like this?*", I would ask each one.

"*Because it makes me happy to see what kind of reactions I can ellict out of you.*" 

Maybe something like this is part of [The Worthwhile Journey](/blog/the-worthwhile-journey/)