---
layout: page
title: "The Joke is Me"
subheadline: "Live, Laugh, Love"
teaser: "Happy April Fools' Day!!!"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/cautious-hero.jpg
    title: full/cautious-hero.jpg
    thumb:  thumb/cautious-hero.jpg
---
As the unofficial worldwide holiday for pranks and wacky antics, I take the time to give thanks for the fact that I can laugh at all. 

If possible, I try to surround myself with people who can also be in good humour, because they have the tendency to look at the best of every situation, good or bad. 

Why is that? 

My theory is that there was a time in their life where all was not sunshine and roses, and getting out of that situation led them to appreciating the goofiness of everyday living. 

> I hope you have a good laugh today too - feel free to share your favorite joke or story in the comments below!

### Trivia 
Today's banner image is from the anime `Cautious Hero`

You find out in episode 11 why I chose it as today's picture :^) 