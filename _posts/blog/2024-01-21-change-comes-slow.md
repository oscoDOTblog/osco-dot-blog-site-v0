---
layout: page
subheadline: "Tomorrow is always much closer than we realize"
title: "Change Comes Slow"
teaser: "Where were you just a year ago?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/asuna-lake.jpeg
    title: full/asuna-lake.jpeg
    thumb:  thumb/asuna-lake.jpg
---
About a year ago today, I was feeling lost.

I was unhappy in my lot in life, despite doing everything "right."

By chance, I stumbled into [Buildspace](https://buildspace.so/), a free online school where you spend 6 weeks working on your idea. 

As it turns out, I had [many](/projects).

But as it might stand, change does not happen overnight.

There is no great sage or person of destiny or calling from the wind to enable your fate.

It's just you and your willingness to take the first step into the unknown.

Your unknown.