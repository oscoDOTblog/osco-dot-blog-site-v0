---
layout: page
subheadline: "Nothing worth having in life is easy"
title: "The Worthwhile Journey"
teaser: "If you knew how difficult the journey would be, would you still take the first step?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-kiriasu-floor75.jpeg
    title: full/sao-kiriasu-floor75.jpeg
    thumb:  thumb/sao-kiriasu-floor75.jpg
---
A while ago, I saw a video titled [Jensen Huang : Don't Start A StartUp](https://vid.puffyan.us/watch?v=wH4cv1e1MvU). 

It shatters the romanticism of entrepreneurship, where the veil is lifted and what stands before you is a lifetime of hardship and obstacles to constantly overcome - that YOU decide to undergo, until you do not. 

If you want an easy life, just get a job under someone else.

But if you want a meaningful life, maybe the struggle is the worthwhile journey. 