---
layout: page
title: "Structure"
subheadline: "Being told what to do is something we take for granted"
teaser: "With all the time in the world, how do you choose to spend it?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/sao-class.jpg
    title: full/sao-class.jpg
    thumb:  thumb/sao-class.jpg
mediaplayer: true
---
It's been about three weeks since I departed from my corporate job, and I've honestly done nothing but read light novels and manga. 

It was something that I forgot I loved to do, but I always felt like I could never justify time to dedicate to it. 

I always thought I would get straight to work after removing the mental burden of a corporate job, but I realized I took the structure of having a 9-5 for granted.

Looks like its time for me now to create my own 7-5.

### Trivia
This blog post title is loosely based on [Structure by InnerPartySystem](https://vid.puffyan.us/watch?v=hnSHnSnaqgs). 

They are one of my favorite bands of all time that I discovered over a decade ago - it's crazy how I can still draw back on how I used to feel back then. 