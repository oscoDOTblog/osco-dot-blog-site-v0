---
layout: page
title: "There is Always Something"
subheadline: "An Elusive Dream for the Ideal Scenario"
teaser: "How much you would get done if you had zero distractions in life?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/dungeon-of-black-company.jpg
    title: full/dungeon-of-black-company.jpg
    thumb:  thumb/dungeon-of-black-company.jpg
---
This marks the first week of being unemployed from my corporate job. 

I always dreamed of a situation where I did NOT have a full-time job, so then I can work full-time on my projects and hobbies. 

I always made the 9-5 my excuse for not being farther ahead in my projects, but something recently dawned on me. 

Even if I didn't have a full-time job, I still have people that want to spend time with me, and as someone who claims to be present in the moment, I always accept the request. 

These outings take time, it actually seems like the 9-5 protected me by making me artificially busy. 

Do I regret time with others?

Not in particular, but the looming dread of running out of time without finishing everything I want to do hangs over me everyday. 

What is the true cost?

### Trivia
Today's image is from `The Dungeon of Black Company`, an anime where the main character succeeds with all their goals in life, and is magically whisked away to start at rock bottom in a new world. 

Does he complain about losing everything?

Yes, but but he gets to work to make sure he gets back to the top once more. 

