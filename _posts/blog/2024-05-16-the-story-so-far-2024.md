---
layout: page
title: "The Story So Far"
subheadline: "The Journey Until Now"
teaser: "It occurred to me that not everyone knows the lore of this blog..."
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/osco-ep1-thumb.jpg
    title: full/osco-ep1-thumb.jpg
    thumb:  thumb/osco-ep1-raw.jpg
mediaplayer: true
---
As much as I loveeee writing, I want to improve my oral storytelling.

I felt like like I have always been a great listener, but terrible at explaining myself in compelling and memorable ways.

So... with that being said, I decided to try my hand at telling various recent and ancient stories from my life via a video blog!

But there is a twist... since I love virtual reality so much, why not try to record my vlogs in the metaverse?

Here is my first attempt at doing so :^)

(The audio is choppy and the stories are all over the place, but that's good! It gives me obvious places to improve for next time~)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ruFh78luUzg?si=YHrrTiPfXYXXnIVj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<!-- TODO: If the above link does not work for some reason, consider watching the backup Odysee link by clicking here.  -->

### Updates
I finished my story outline! Here it is as proof: 

![The Grand Challenge - Story Draft Version 1!](/images/full/tgc-story-draft.jpg)

Next week, I plan to ***create the environments*** for Chapter 1 so I can start building out the next interactive experience!  
