---
layout: page
title: "One Moment and Forever"
subheadline: "Meeting and Parting"
teaser: "Do you make the most of each passing moment?"
categories:
    - blog/
tags:
  - blog
author: Osco
show_meta: true
comments: true
header:
   image_fullwidth  : header/blog-post-1500x300.jpg
image:
    homepage: full/one-moment-and-forever.png
    title: full/one-moment-and-forever.png
    thumb:  thumb/one-moment-and-forever.jpg
mediaplayer: true
---
I've been wrestling with the idea of what it means to be maintaining your relationships. 

As we grow older, we start to accumulate an evergrowing amount of responsibilities, and our time to spend with others continues to dwindle. 

While looking for my own answer, I stumbled across this video once more...

<center>
<video src="https://www.osco.blog/images/full/accel-world-one-moment-and-forever.mp4" 
  width="480" 
  height="360"
  >
</video>
</center>
<br/>

It's a clip from one of my favorite animes called ***Accel World***, where the premise is your unconscious fears and desires create your battle avatar in a digital world, and you use that form to overcome your weaknesses in the real world. 

(*It's actually a HUGE inspiration for the [The Grand Challenge](https://tgc.atos.to), a 3D interactive story I'm working on!*)

What strikes me about this clip is the innate hesitation and assumption that we do not need to resolve issues that we have in our relationships with others, because we will definitely get the chance to see them again and fix it then. 

***... But what if we don't?***

What relationships should we maintain continually? 

What relationships should we leave in the past?

And the most difficult of... what relationships should we dig out of the past and resolve?

***We always have less time than we realize...***

So let's make the most out of it with the people we care about.

> We are all living in the gap between one moment and forever. 

> Meeting and parting, in a world where everything changes and everyone is frantically living their life. 

> There must be some things that don't change.

> But there are some things that change and can never be got back. 

> But... we already know. 

> Even if things do change... things start over again... 

> And... as long as as they have the desire... people can always move forward... 

> And they accelerate... into a new world!